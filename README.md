<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Roles Permission
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-roles-permission.git`
2. Go inside the folder: `cd laravel-7-roles-permission`
3. Run `cp .env.example .env` then put your DB name & credentials
4. Run `composer install`
5. Run `php artisan key:generate`
5. Run `php artisan migrate`
6. Run `php artisan db:seed`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

|#|Email|Role|
|---|---|---|
|1|uzumaki_naruto@konohagakure.co.jp|Admin|
|2|uchiha_sasuke@konohagakure.co.jp|User|
